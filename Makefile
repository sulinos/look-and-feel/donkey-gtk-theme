
all: dummy

dummy:
	true

install:
	mkdir -p $(DESTDIR)/usr/share/themes/
	cp -r donkey*  $(DESTDIR)/usr/share/themes/
	rm -f $(DESTDIR)/usr/share/themes/Makefile
